require('./model/mongodb')
const courseController = require('./controllers/courseController');

//Import the necessary packages
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

//Create a welcome message and direct them to the main page
app.get('/', (req, res) => {
    res.send('Welcome to our app');
});

//Set the Controller path which will be responding the user actions
app.use('/api/courses', courseController);

//Establish the server connection
//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT || 9080;
app.listen(port, () => console.log(`Listening on port ${port}..`));