const mongoose = require('mongoose');

//Attributes of the Course object
var courseSchema = new mongoose.Schema({
    courseName: {
        type: String,
        required: 'This field is required!'
    },
    courseId: {
        type: String
    },
    author: {
        type: String
    },
    courseDuration: {
        type: String
    },
    courseFee: {
        type: String
    },
    discount: {
        type: Number
    }
});

mongoose.model('Course', courseSchema);